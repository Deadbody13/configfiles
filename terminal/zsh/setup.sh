#!/bin/bash
# moves files into approprate locations

cp .zshrc .zshenv $HOME

git clone --depth=1 https://github.com/romkatv/powerlevel10k.git 
sudo cp -r powerlevel10k /usr/share/zsh/plugins/

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
sudo cp -r zsh-syntax-highlighting /usr/share/zsh/plugins/
